# -*- mode: sh -*-
[ -n "$XDG_SESSION_TYPE" ] && {
    export XDG_CONFIG_HOME="$HOME/.config"
    export XDG_CACHE_HOME="$HOME/.cache"
    export XDG_DATA_HOME="$HOME/.local/share"
    export XDG_DATA_DIRS=/usr/local/share:/usr/share
    export XDG_CONFIG_DIRS=/etc/xdg

    export QT_AUTO_SCREEN_SCALE_FACTOR=1
    export QT_QPA_PLATFORM="wayland;xcb"
    export QT_QPA_PLATFORMTHEME=qt5ct
    export SDL_VIDEODRIVER="wayland,x11"

    export GTK_IM_MODULE=fcitx
    export QT_IM_MODULE=fcitx
    export XMODIFIERS=@im=fcitx

    export CALIBRE_USE_SYSTEM_THEME=1
    export MOZ_ENABLE_WAYLAND=1
    export MOZ_DBUS_REMOTE=1
}

moz_profile() {
    moz_ini="$HOME/.mozilla/firefox-esr/profiles.ini"
    moz_pattern='/^\[Install.*\]$/{n;s/^Default=\(.*\)$/\1/p}'
    eval 'set -- $(sed -n '\'"$moz_pattern"\'' '"$moz_ini"')'
    echo "$1"
}

export PATH="$HOME/.local/bin:$PATH"
export JAVA_HOME=/usr/lib/jvm/default
export PYTHONRC="$HOME/.config/pythonrc"
MY_MOZ_PROFILE="$(moz_profile _)"
[ -n "$MY_MOZ_PROFILE" ] && {
    export MY_MOZ_PROFILE="$HOME/.mozilla/firefox-esr/$MY_MOZ_PROFILE"
}

unset -f moz_profile
#shellcheck disable=SC1090
[ -n "$BASH" ] && [ -z "$POSIXLY_CORRECT" ] && [ -f ~/.bashrc ] && . ~/.bashrc
