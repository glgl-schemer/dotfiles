* How To Use

#+BEGIN_SRC shell
cd $HOME
git clone git@gitlab.com:glgl-schemer/dotfiles.git .config
ln -s .config/bashrc .bashrc
ln -s .config/profile .profile
ln -s .config/xkb .xkb
ln -s ../../../.config/fcitx5/rime .local/share/fcitx5/rime
ln -s ../../.config/paru/git-shallow .local/bin/git-shallow
ln -s ../.config/gpg-agent.conf .gnupg/gpg-agent.conf
ln -s ../../.config/applications .local/share/applications
update-desktop-database $XDG_DATA_HOME/applications
#+END_SRC

* UDev Rules

#+BEGIN_SRC
KERNEL=="uinput", GROUP="input", MODE="0660"
#+END_SRC

* Muttrc

#+BEGIN_SRC conf
set imap_user=$email
set imap_pass="`gpg --batch -q --decrypt .password.gpg`"

set realname=$name
set from=$email
set use_from=yes
set smtp_url=smtp://$imap_user:$imap_pass@$smtp.server:587
set ssl_force_tls=yes
set ssl_starttls=yes
set smtp_authenticators=login

set folder=imaps://$imap.server:993/
set spoolfile=+INBOX
unset record
set postponed=+Drafts
set header_cache=`pwd`

### https://www.redhat.com/sysadmin/mutt-email-oauth2
# set imap_authenticators="oauthbearer:xoauth2"
# set imap_oauth_refresh_command="${header_cache}/mutt_oauth2.py ${header_cache}/token.json.gpg"
# set smtp_authenticators=${imap_authenticators}
# set smtp_oauth_refresh_command=${imap_oauth_refresh_command}

source ~/.config/mutt/common.muttrc
#+END_SRC

* Inputrc

#+BEGIN_SRC conf
set bell-style none

set meta-flag on
set input-meta on
set output-meta on
set convert-meta off
set bind-tty-special-chars off

$if mode=emacs
"\ep": history-search-backward
"\en": history-search-forward
"\C-w": backward-kill-word
"\e\C-y": kill-whole-line
"\e\C-h": unix-word-rubout
"\e\C-?": unix-word-rubout
$endif
#+END_SRC

* Greetd

#+BEGIN_SRC toml
### config.toml
[terminal]
vt = 7

[default_session]
command = "sway --config /etc/greetd/sway.cfg"
# useradd --system -d /var/lib/greetd -G video -s /sbin/nologin -c "Greetd User" greeter
user = "greeter"

### i3rs.toml
[theme]
theme = "srcery"

[[block]]
block = "custom"
command = "true"
format = " Reboot "
[[block.click]]
button = "left"
cmd = "loginctl reboot"
[block.theme_overrides]
idle_bg = "#398EE7"
idle_fg = "#E0E0E0"

[[block]]
block = "custom"
command = "true"
format = " Poweroff "
[[block.click]]
button = "left"
cmd = "loginctl poweroff"
[block.theme_overrides]
idle_bg = "#114F8F"
idle_fg = "#E0E0E0"
#+END_SRC

#+BEGIN_SRC conf
### sway.cfg
seat * xcursor_theme Adwaita 48
exec gsettings set org.gnome.desktop.interface cursor-theme Adwaita
exec gsettings set org.gnome.desktop.interface cursor-size 48

bar {
    font sans 24
    output *
    pango_markup enabled
    position top
    status_command i3status-rs /etc/greetd/i3rs.toml
    workspace_buttons no

    colors {
        background B5B5B5FF
    }
}

exec "gtkgreet -l -s /etc/greetd/style.css -c /etc/greetd/sway-run; swaymsg exit"
#+END_SRC

#+BEGIN_SRC css
/* style.css */

 * {
    font-size: 36px;
}

.background {
    background: #B5B5B5;
    color: #222222;
}

button.suggested-action {
    background: #114F8F;
    border: none;
}

button.suggested-action label {
    background: #114F8F;
    color: #E0E0E0;
}

combobox>window.popup>menu menuitem{
    background: #114F8F;
    color: #E0E0E0;
}

entry {
    border: none;
}
#+END_SRC

#+BEGIN_SRC shell
### sway-run
#!/bin/sh

export XDG_SESSION_TYPE=wayland

[ -z "$DBUS_SESSION_BUS_ADDRESS" ] && eval "$(dbus-launch --sh-syntax --exit-with-session)"
[ -f /etc/profile ] && . /etc/profile
[ -f "$HOME"/.profile ] && . "$HOME"/.profile

exec sway
#+END_SRC

* ACPID

#+BEGIN_SRC shell
### Ignore Handle* in logind.conf

### /etc/acpi/events/lid-button
event=button/lid.*
action=/etc/acpi/actions/lid-button.sh %e

### /etc/acpi/events/lid-button.sh
#!/bin/sh

case "$3" in
    close)
        logger 'LID closed'
        if [ $(grep ^connected /sys/class/drm/card*-*-*/status | wc -l) -eq 1 ]
        then
            loginctl lock-sessions
        fi
        ;;
    open)
        logger 'LID opened'
        ;;
    *)
        logger "ACPI action undefined: $3"
        ;;
esac
#+END_SRC
