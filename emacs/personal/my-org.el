;; my-org --- -*- lexical-binding: nil -*-

;;; Commentary:

;;; Code:

(require 'ox-beamer)
(dolist (cell '(("only" "O" "\\begin{onlyenv}%a" "\\end{onlyenv}")
                ("uncover" "U" "\\begin{uncoverenv}%a" "\\end{uncoverenv}")))
  (add-to-list 'org-beamer-environments-extra cell))

(require 'ox-latex)
(add-hook 'org-mode-hook 'org-indent-mode)
(set-face-attribute 'org-document-title nil :height 1.0)
(dolist (key '("C-'" "M-RET")) (define-key org-mode-map (kbd key) nil))
(setq org-latex-src-block-backend 'listings org-latex-prefer-user-labels t
      org-latex-default-packages-alist
      '(("" "listings" nil) ("" "hyperref" nil))
      org-latex-pdf-process
      '("xelatex -interaction nonstopmode -halt-on-error -output-directory %o %f"
        "bibtex %b"
        "xelatex -interaction nonstopmode -halt-on-error -output-directory %o %f"
        "xelatex -interaction nonstopmode -halt-on-error -output-directory %o %f"))

(dolist (cell '(("ctexart" "\\documentclass{ctexart}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
                ("ctexrep" "\\documentclass{report}"
                 ("\\part{%s}" . "\\part*{%s}")
                 ("\\chapter{%s}" . "\\chapter*{%s}")
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
                ("ctexbook" "\\documentclass{book}"
                 ("\\part{%s}" . "\\part*{%s}")
                 ("\\chapter{%s}" . "\\chapter*{%s}")
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))
                ("ctexbeamer" "\\documentclass{ctexbeamer}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}"))))
  (add-to-list 'org-latex-classes cell))

(add-to-list 'org-export-filter-headline-functions
             (lambda (string backend info)
               (if (string-match "\\`.*ignoreheading.*\n" (downcase string))
                   (replace-match "" nil nil string))))

(advice-add 'org-latex--org-table :around
            (lambda (fun table contents i)
              (let* ((attr (org-export-read-attribute :attr_latex table))
                     (alignment (org-latex--align-string table i))
                     (env (or (plist-get attr :environment)
                              (plist-get i :latex-default-table-environment)))
                     (anchor (plist-get attr :anchor))
                     (str (funcall fun table contents i)))
                (if (not anchor) str
                  (replace-regexp-in-string
                   (concat "\\(\\\\begin{" env "}\\)\\(.*\n?\\)*\\'")
                   (concat "\\\\begin{" env "}[" anchor "]")
                   str nil nil 1)))))

(advice-add 'org-element-latex-fragment-parser :filter-return
            (lambda (latex-fragment)
              (when latex-fragment
                (let* ((plist (cadr latex-fragment))
                       (b (plist-get plist :begin))
                       (e (plist-get plist :end))
                       (p (plist-get plist :post-blank)))
                  (if (or (not (eq ?\\ (char-after b)))
                          (memq (char-after (1+ b)) `(,?\[ ,?\()))
                      latex-fragment
                    (goto-char e)
                    (while (and (memq (char-after (point)) `(,?\[ ,?\{ ,?<))
                                (let ((next (scan-sexps (point) 1)))
                                  (when next (goto-char next)))))
                    (let* ((e (point)) (v (filter-buffer-substring b e)))
                      (goto-char b)
                      `(latex-fragment
                        (:value ,v :begin ,b :end ,e :post-blank ,p))))))))

(provide 'my-org)
;;; my-org.el ends here
