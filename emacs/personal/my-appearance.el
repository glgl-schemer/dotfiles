;; my-appearance --- -*- lexical-binding: nil -*-

;;; Commentary:

;;; Code:

(add-hook 'after-make-frame-functions
          (lambda (frame)
            (set-face-attribute
             'default frame
             :font (font-spec :family "Noto Sans Mono CJK SC" :size 28))))

(define-fringe-bitmap 'right-arrow
  [ #b00000000 #b00000000 #b11000000 #b11000000
    #b00000000 #b00000000 #b11000000 #b11000000
    #b00000000 #b00000000 #b11000000 #b11000000
    #b00000000 #b00000000 #b11000000 #b11000000
    #b00000000 #b00000000])
(define-fringe-bitmap 'left-arrow
  [ #b00000000 #b00000000 #b00000011 #b00000011
    #b00000000 #b00000000 #b00000011 #b00000011
    #b00000000 #b00000000 #b00000011 #b00000011
    #b00000000 #b00000000 #b00000011 #b00000011
    #b00000000 #b00000000])
(define-fringe-bitmap 'right-curly-arrow
  [ #b00000000 #b00000000 #b11000000 #b11000000
    #b00000000 #b00000000 #b11000000 #b11000000
    #b00000000 #b00000000 #b11000000 #b11000000
    #b00000000 #b00000000 #b11000000 #b11000000
    #b00000000 #b00000000])
(define-fringe-bitmap 'left-curly-arrow
  [ #b00000000 #b00000000 #b00000011 #b00000011
    #b00000000 #b00000000 #b00000011 #b00000011
    #b00000000 #b00000000 #b00000011 #b00000011
    #b00000000 #b00000000 #b00000011 #b00000011
    #b00000000 #b00000000])

(dolist (item '((alpha . (96 . 96)))) (add-to-list 'default-frame-alist item))

(dolist (mode '(menu-bar tool-bar scroll-bar electric-indent))
  (funcall (intern (concat (symbol-name mode) "-mode")) -1))

(setcdr (cdddr mode-line-modes) (cdddr (cddr mode-line-modes)))

(let ((custom-theme-load-path `(,(concat user-emacs-directory "personal"))))
  (unless (and (string-equal (getenv "TERM") "linux") (not (getenv "DISPLAY")))
    (load-theme 'my-base16 t)))

(provide 'my-appearance)
;;; my-appearance.el ends here
