;; my-base16-theme --- -*- lexical-binding: nil -*-

;;; Commentary:

;;; Authors:

;;; Code:

(require 'base16-theme)

(deftheme my-base16)

(let ((colors '(:base00 "#c6c6c6"
                :base01 "#86c686"
                :base02 "#c6c686"
                :base03 "#868686"
                :base04 "#6686a6"
                :base05 "#222222"
                :base06 "#a686c6"
                :base07 "#000000"
                :base08 "#862222"
                :base09 "#a65544"
                :base0A "#666600"
                :base0B "#226622"
                :base0C "#86c6c6"
                :base0D "#002286"
                :base0E "#662286"
                :base0F "#226666"))
      (faces '((ansi-color-black   :foreground base07 :background base05)
               (ansi-color-white   :foreground base03 :background base00)
               (ansi-color-red     :foreground base08 :background base09)
               (ansi-color-yellow  :foreground base0A :background base02)
               (ansi-color-green   :foreground base0B :background base01)
               (ansi-color-cyan    :foreground base0F :background base0C)
               (ansi-color-blue    :foreground base0D :background base04)
               (ansi-color-magenta :foreground base0E :background base06)
               (company-preview                       :foreground base0D)
               (company-tooltip-common                :foreground base08)
               (completions-common-part               :foreground base0F)
               (ediff-odd-diff-A   :foreground base0D :inverse-video t)
               (ediff-odd-diff-B   :foreground base0D :inverse-video t)
               (ediff-odd-diff-C   :foreground base0D :inverse-video t)
               (eshell-ls-archive  :foreground base09)
               (eshell-ls-backup   :foreground base0A)
               (eshell-ls-product  :foreground base0A)
               (eshell-ls-readonly :foreground base0E)
               (eshell-ls-symlink  :foreground base0F)
               (eshell-ls-unreadable                  :foreground base0E)
               (font-lock-builtin-face                :foreground base0F)
               (font-lock-comment-delimiter-face      :foreground base0A)
               (font-lock-comment-face                :foreground base0D)
               (font-lock-doc-face                    :foreground base0D)
               (flymake-warnline   :background base00 :underline base08)
               (flymake-errline    :background base02 :underline base08)
               (flymake-warning    :background base00 :underline base08)
               (flymake-error      :background base02 :underline base08)
               (flymake-note       :background base00 :underline base0B)
               (fringe             :background base03)
               (grep-context-face  :foreground base0E)
               (gui-element        :background base00)
               (ido-subdir         :foreground base0D)
               (ido-virtual        :foreground base0D)
               (isearch            :foreground base0B :background base00)
               (isearch-fail       :background base02 :inverse-video t)
               (lazy-highlight     :foreground base0C :background base07 :inverse-video t)
               (line-number        :foreground base0A :background base00)
               (line-number-current-line              :foreground base07)
               (mode-line          :background base00 :box nil)
               (mode-line-active   :foreground base0D)
               (mode-line-emphasis :foreground base0E :slant italic)
               (mode-line-inactive :foreground base05)
               (org-block-begin-line       :foreground base0A :background base00)
               (org-block          :foreground base05 :background base00)
               (org-column         :background base00)
               (org-document-info  :foreground base0F)
               (org-drawer         :foreground base0B)
               (org-ellipsis       :foreground base0D)
               (org-footnote       :foreground base0F)
               (popup-face                 :foreground base07 :background base01)
               (popup-menu-mouse-face      :foreground base07 :background base0C)
               (popup-menu-selection-face  :foreground base07 :background base02)
               (popup-summary-face         :foreground base0D)
               (popup-tip-face             :foreground base07 :background base01)
               (region             :distant-foreground base07 :background base0C)
               (tab-bar-tab                :foreground base09 :background base00)
               (tab-bar-tab-inactive       :foreground base0E :background base00)
               (tab-line-highlight :distant-foreground base05 :background base00)
               (term-color-black   :foreground base07 :background base05)
               (term-color-white   :foreground base03 :background base00)
               (term-color-red     :foreground base08 :background base09)
               (term-color-yellow  :foreground base0A :background base02)
               (term-color-green   :foreground base0B :background base01)
               (term-color-cyan    :foreground base0F :background base0C)
               (term-color-blue    :foreground base0D :background base04)
               (term-color-magenta :foreground base0E :background base06)))
      (tty-colors '(:base00 "brightwhite" :base03 "white" :base07
                            "brightblack" :base05 "black")))
  (letrec ((putrec (lambda (overrides)
                     (if (not overrides) base16-theme-shell-colors
                       (plist-put (funcall putrec (cddr overrides))
                                  (car overrides) (cadr overrides))))))
    (setq base16-theme-shell-colors (funcall putrec tty-colors)))
  (base16-theme-set-faces 'my-base16 colors faces)
  (base16-theme-define 'my-base16 colors))

(provide-theme 'my-base16)

(provide 'my-base16-theme)
;;; my-base16-theme.el ends here
