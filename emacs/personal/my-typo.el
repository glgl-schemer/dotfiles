;; my-typo --- -*- lexical-binding: nil -*-

;;; Commentary:

;;; Code:

(require 'ispell)
(add-hook 'text-mode-hook 'ispell-minor-mode)
(make-variable-buffer-local 'ispell-skip-region-alist)
(put 'ispell-personal-dictionary 'safe-local-variable 'stringp)
(advice-add 'ispell-find-hunspell-dictionaries :override (lambda () nil))
(add-hook 'ispell-minor-mode-hook (lambda () (ispell-change-dictionary "en")))
(setq ispell-local-dictionary "en" ispell-personal-dictionary null-device
      ispell-local-dictionary-alist
      '(("en" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "en_GB-large") nil
         utf-8))
      ispell-hunspell-dict-paths-alist
      '(("en" "/usr/share/hunspell/en_GB-large.aff")))

(require 'org-element)
(add-hook 'org-mode-hook
          (lambda ()
            (let ((tex (lambda ()
                         (backward-char)
                         (let ((item (ignore-error t
                                       (org-element-latex-environment-parser
                                        nil `(,(point))))))
                           (if item (goto-char (plist-get (cadr item) :end))
                             (let ((item (org-element-latex-fragment-parser)))
                               (if (not item) (forward-char)
                                 (goto-char (plist-get (cadr item) :end))
                                 (when (= (char-before) ?\})
                                   (goto-char (scan-sexps (point) -1))))))))))
              (dolist (form `(("\\\\" ,tex)
                              ("\\(^\\|[[:space:]'({\"]\\)="
                               "=\\($\\|[[:space:],:;!')}\"\\.\\?\\-\\\\]\\)")
                              ("\\(^\\|[[:space:]'({\"]\\)~"
                               "~\\($\\|[[:space:],:;!')}\"\\.\\?\\-\\\\]\\)")
                              ("^#\\+begin_example[^:]" "^#\\+end_example[^:]")
                              ("^#\\+begin_export[^:]" "^#\\+end_export[^:]")
                              ("^#\\+begin_src[^:]" "^#\\+end_src[^:]")
                              ("\\[\\[" "]\\(]\\|\\[\\)")
                              ("^#\\+[^[:space:]]+:.*$")
                              (":[[:alpha:]_]+:.*$")))
                (add-to-list 'ispell-skip-region-alist form)))))

(provide 'my-typo)
;;; my-typo.el ends here
