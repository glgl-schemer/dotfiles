;; my-programming --- -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(require 'company)
(make-variable-buffer-local 'company-backends)
(define-key company-active-map (kbd "C-h") nil)
(setq company-echo-truncate-lines nil)
(dolist (hook '(prog-mode-hook eshell-mode-hook comint-mode-hook))
  (add-hook hook 'company-mode-on))

(require 'diff-mode)
(dolist (key '("M-o" "M-RET")) (define-key diff-mode-map (kbd key) nil))

(require 'eglot)
(let ((jdtls (lambda (&rest _)
               (let* ((sub "[^/:]+:[^/:]*")
                      (p (concat "\\`/\\(\\(" sub  "|\\)*" sub ":\\)"))
                      (p (when (string-match p default-directory)
                           (match-string 0 default-directory)))
                      (home (expand-file-name (concat p "~/")))
                      (home (if p (substring home (length p)) home))
                      (w (concat "/tmp/eglot-eclipse-jdt-cache"
                          (md5 (project-root (eglot--current-project))))))
                 (unless (file-directory-p w) (make-directory w t))
                 `(,(concat home ".config/emacs/java/bin/jdtls")
                   "-configuration" "/tmp/jdtls"
                   "-data" ,w)))))
  (setq eglot-server-programs
        `(((c-mode c++-mode) .
           ("clangd" "--header-insertion=never" "--completion-style=detailed"
            "--all-scopes-completion=false" "--background-index=false"
            "--clang-tidy=false"))
          (rust-ts-mode . ("rust-analyzer"))
          (text-mode . ("ltex-ls"))
          (java-mode . ,jdtls)))
  (dolist (hook '(org-mode-hook rst-mode-hook mail-mode-hook))
    (add-hook hook 'eglot-ensure)))

(require 'project)
(make-variable-buffer-local 'project-find-functions)

(require 'smartparens-config)
(setq sp-escape-quotes-after-insert nil)
(dolist (mode '(smartparens-mode show-smartparens-mode))
  (dolist (hook '(prog-mode-hook comint-mode-hook))
    (add-hook hook mode)))
(dolist (cf '(("s" sp-splice-sexp) ("r" sp-rewrap-sexp) ;; common
              ("," sp-forward-barf-sexp) ("." sp-forward-slurp-sexp) ;; lisps
              ("[" sp-backward-up-sexp) ("]" sp-down-sexp)
              ("/" sp-slurp-hybrid-sexp) ;; non lisps
              ("-" sp-backward-sexp) ("=" sp-forward-sexp)))
  (define-key smartparens-mode-map (kbd (concat "M-o " (car cf))) (cadr cf)))

(require 'yasnippet)
(add-hook 'company-mode-hook 'yas-minor-mode)
(add-hook 'yas-minor-mode-hook 'yas-reload-all)

(require 'compile)
(let ((p (lambda (s)
                (or (not (stringp s)) (zerop (length s)) (string-blank-p s))))
      (doc-buffer (lambda (s)
                    (save-excursion
                      (with-current-buffer (help-buffer)
                        (erase-buffer)
                        (insert s)
                        (unless compilation-minor-mode (compilation-minor-mode))
                        (display-buffer (current-buffer))))))
      match-message position-changed)
  (advice-add 'sp-show--pair-echo-match :filter-args
              (lambda (match-positions)
                (setq position-changed
                      (not (and (equal
                      sp-show-pair-previous-point (point))
                                (equal sp-show-pair-previous-match-positions
                                       match-positions))))
                match-positions))
  (advice-add 'sp-show--pair-echo-match :filter-return
              (lambda (message)
                (when position-changed (setq match-message message))))
  (advice-add 'sp-show--pair-delete-overlays :before
              (lambda (&rest _rest) (setq match-message nil)))
  (advice-add 'eldoc-minibuffer-message :filter-args
              (lambda (args) ;; company and sp will not echo simultanously
                (let ((msg (concat match-message company-echo-last-msg)))
                  (if (or (funcall p msg) (minibufferp)) args
                    (let ((format (car args)) (rest (cdr args)))
                      (if (funcall p (apply 'message format rest))
                          `(,msg) (cons (concat msg " | " format) rest)))))))
  (advice-add 'eldoc-doc-buffer :around
              (lambda (orig-fun &rest _rest)
                (interactive)
                (let ((inhibit-read-only t)
                      (s (help-at-pt-string))
                      (e (and (buffer-live-p eldoc--doc-buffer)
                              (with-current-buffer eldoc--doc-buffer
                                (not (string-blank-p (buffer-string)))))))
                  (cond (company-candidates (company-show-doc-buffer))
                        (s (funcall doc-buffer s))
                        (e (funcall orig-fun))))))
  (advice-add 'display-local-help :override 'eldoc-doc-buffer))

(require 'cc-mode)
(let ((fmodes '(("_EDITMSG\\'" . my/msg-mode) ("\\.rs" . rust-ts-mode)))
      (hook (lambda ()
              (delete 'company-capf company-backends)
              (push 'company-capf company-backends)
              (eglot-inlay-hints-mode -1)
              (when (and (member (file-name-extension (buffer-file-name))
                                 '("y" "yacc" "lex"))
                         (eglot-managed-p))
                (eglot--managed-mode-off)))))
  (dolist (fmod fmodes) (add-to-list 'auto-mode-alist fmod))
  (add-hook 'eglot-managed-mode-hook hook))

(require 'cmake-mode)
(let ((flymake-info '(:eval (ignore-errors (flymake--mode-line-counters)))))
  (add-hook 'prog-mode-hook 'flymake-mode)
  (add-hook 'flymake-mode-hook
            (lambda () (add-to-list 'mode-line-misc-info flymake-info)))
  ;; `display-local-help' shows flymake dignostic at point
  (define-key flymake-mode-map (kbd "M-n") 'flymake-goto-next-error)
  (define-key flymake-mode-map (kbd "M-p") 'flymake-goto-prev-error)
  (setq elisp-flymake-byte-compile-load-path
        (append elisp-flymake-byte-compile-load-path load-path))
  (advice-add 'elisp-get-fnsym-args-string :around
              (lambda (orig-fun sym &rest args)
                (concat ;; disable minibuffer resize for multiline
                 (apply orig-fun sym args)
                 (when sym
                   (let ((d (ignore-errors (documentation sym 'raw))))
                     (and d (concat " | " (propertize d 'face 'italic)))))))))

(require 'rust-ts-mode)
(let* ((sh (lambda (report-fn &rest _rest)
             (when (executable-find "shellcheck")
               (let* (point shebang
                      (src (current-buffer))
                      (cmd '("shellcheck" "-S" "info" "-f" "gcc" "-s" "sh" "-"))
                      (f (lambda ()
                           (cl-loop
                            repeat 99
                            while (string-match "\
^-:\\([0-9]+\\):\\([0-9]+\\): \\(.*\\)$" (buffer-string) point)
                            collect
                            (let* ((l (string-to-number
                                       (match-string 1 (buffer-string))))
                                   (l (if (< shebang l) (1- l) l))
                                   (c (string-to-number
                                       (match-string 2 (buffer-string))))
                                   (r (flymake-diag-region src l c))
                                   (m (match-string 3 (buffer-string))))
                              (setq point (match-end 0))
                              (flymake-make-diagnostic
                               src (car r) (cdr r) :warning m)))))
                      (f (lambda (proc _event)
                           (when (eq 'exit (process-status proc))
                             (with-current-buffer (process-buffer proc)
                               (funcall report-fn (funcall f) :force t)
                               (kill-buffer)))))
                      (proc (make-process :name "sh" :noquery t :sentinel f
                                          :buffer (generate-new-buffer "*sh*")
                                          :connection-type 'pipe :command cmd)))
                 (with-current-buffer src
                   (save-excursion
                     (goto-char (point-min))
                     (if (not (search-forward "#!" nil t)) (setq shebang 0)
                       (if (search-forward "!#" nil t)
                           (setq shebang (count-lines (point-min) (point)))
                         (setq shebang 1)
                         (move-end-of-line nil))
                       (forward-char))
                     (process-send-region proc (point-min) (point))
                     (process-send-string proc "#shellcheck source=/dev/null\n")
                     (process-send-region proc (point) (point-max))
                     (process-send-eof proc)))))))
       (fmt (lambda () (interactive) (when (eglot-managed-p) (eglot-format))))
       (sh-on (lambda () (setq-local flymake-diagnostic-functions `(,sh)))))
  (dolist (map `(,c-mode-map ,c++-mode-map ,java-mode-map ,rust-ts-mode-map))
    (define-key map (kbd "M-q") fmt))
  (add-hook 'sh-mode-hook sh-on))

(require 'sh-script)
(let* ((p (lambda (file)
            (letrec ((self
                      (lambda (d)
                        (let* ((d (directory-file-name (expand-file-name d)))
                               (sub "[^/:]+:[^/:]*")
                               (pattern (concat "\\`/\\(\\(" sub  "|\\)*" sub
                                           ":/\\)?\\'"))
                               (p (concat "\\`/\\(" sub  "|\\)*" sub ":")))
                          (when (and d (not (string-match pattern d)))
                            (if (file-readable-p (concat d "/" file))
                                (cons 'transient d)
                              (let ((home (expand-file-name
                                           (concat (when (string-match p d)
                                                     (match-string 0 d))
                                                   "~")
                                           t)))
                                (unless (string-equal d home)
                                  (funcall self (file-name-directory d))))))))))
              self)))
       (s (lambda ()
            (when (executable-find "shfmt" t)
              (let* ((find-ec (funcall p ".editorconfig"))
                     (ec (funcall find-ec default-directory)))
                (lambda ()
                  (interactive)
                  (let ((p (make-process
                            :name "shfmt" :connection-type 'pipe :file-handler t
                            :buffer (generate-new-buffer "*shfmt*") :noquery t
                            :command `("shfmt"
                                       ,@(unless ec '("-i=4" "-p" "-ci" "-sr"))
                                       ,@(when (and buffer-file-name ec)
                                           (save-buffer)
                                           `(,buffer-file-name)))
                            :sentinel (lambda (proc _event)
                                        (when (eq 'exit (process-status proc))
                                          (let* ((b (process-buffer proc)))
                                            (replace-buffer-contents b)
                                            (kill-buffer b)))))))
                    (unless (and buffer-file-name ec)
                      (process-send-region p (point-min) (point-max))
                      (process-send-eof p))))))))
       (cls (lambda ()
              (when (executable-find "clang-format" t)
                (let ((buffer (current-buffer)))
                  (make-process
                   :name "clang-format" :connection-type 'pipe :file-handler t
                   :buffer (generate-new-buffer "*clang-format*") :noquery t
                   :command '("clang-format" "--dump-config")
                   :sentinel
                   (lambda (proc _event)
                     (when (eq 'exit (process-status proc))
                       (let* ((b (process-buffer proc))
                              (s (with-current-buffer b (buffer-string)))
                              (w (progn (string-match
                                         "^TabWidth:[^0-9]*\\([0-9]+\\)$" s)
                                        (string-to-number (match-string 1 s))))
                              (o (progn (string-match
                                         "^IndentWidth:[^0-9]*\\([0-9]+\\)$" s)
                                        (string-to-number (match-string 1 s)))))
                         (with-current-buffer buffer
                           (setq-local tab-width w c-basic-offset o))
                         (kill-buffer b)))))))
              (when (file-remote-p (or (executable-find "clang" t) "/sudo::"))
                (delete 'company-clang company-backends))
              (push (funcall p "compile_commands.json") project-find-functions)
              (push (funcall p "compile_flags.txt") project-find-functions)))
       (jls (lambda ()
              (setq-local
               eglot-workspace-configuration
               '((:java :settings (:url "~/.config/emacs/settings.prefs")
                        :import (:maven (:offline (:enabled :json-false)))
                        :eclipse (:downloadSources :json-false)
                        :maven (:downloadSources :json-false)
                        :autobuild (:enabled :json-false)
                        :format (:enabled :json-false))))
              (letrec ((b (current-buffer))
                       (conf (lambda (s)
                               (with-current-buffer b
                                 (eglot-signal-didChangeConfiguration s))
                               (remove-hook 'eglot-connect-hook conf))))
                (add-hook 'eglot-connect-hook conf))
              (push (funcall p ".project") project-find-functions)
              (push (funcall p "pom.xml") project-find-functions)))
       (rls (lambda ()
              (setq-local
               eglot-workspace-configuration
               '(:rust-analyzer ( :cargo ( :buildScripts (:enable t)
                                           :features "all")
                                  :check ( :command "clippy")
                                  :procMacro (:enable t))))
              (push (funcall p "Cargo.toml") project-find-functions)))
       (s (lambda ()
            (let ((shfmt (funcall s)))
              (when shfmt
                (use-local-map (copy-keymap sh-mode-map))
                (local-set-key (kbd "M-q") shfmt))))))
  (dolist (hookls `((c-mode-hook ,cls) (c++-mode-hook ,cls)
                    (java-mode-hook ,jls) (rust-ts-mode-hook ,rls)))
    (add-hook (car hookls) (lambda () (funcall (cadr hookls)) (eglot-ensure))))
  (define-key prog-mode-map (kbd "M-q") (lambda () nil))
  (add-hook 'sh-mode-hook s))

(cl-defmethod eglot-execute-command
  (_server (_cmd (eql java.apply.workspaceEdit)) arguments)
  "Eclipse JDT breaks spec and replies with edits as ARGUMENTS."
  (mapc #'eglot--apply-workspace-edit arguments))

(define-derived-mode my/msg-mode text-mode "MSG" "."
  (eglot-ensure)
  (setq-local comment-start "\n#"))

(provide 'my-programming)
;;; my-programming.el ends here
