;; my-package --- -*- lexical-binding: nil -*-

;;; Commentary:

;;; Code:

(require 'package)
(setq load-prefer-newer t
      package-user-dir (expand-file-name "~/.local/share/emacs/elpa")
      package-archives
      '(("gnu"   . "https://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
        ("melpa" . "https://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/"))
      package-selected-packages
      '(auto-compile
        base16-theme company popup rime smartparens undo-fu vterm yasnippet))

(package-initialize)
(dolist (package package-selected-packages)
  (unless (package-installed-p package)
    (unless (ignore-error (package-install package))
      (package-refresh-contents)
      (package-install package))))

(require 'auto-compile)
(auto-compile-on-save-mode)
(auto-compile-on-load-mode)
(setq auto-compile-display-buffer nil byte-compile-warnings nil)

(require 'treesit)
(let ((treesit-language-source-alist
       '((rust "https://github.com/tree-sitter/tree-sitter-rust"))))
  (dolist (lang-src treesit-language-source-alist)
    (unless (treesit-language-available-p (car lang-src))
      (treesit-install-language-grammar (car lang-src)))))

(provide 'my-package)
;;; my-package.el ends here
