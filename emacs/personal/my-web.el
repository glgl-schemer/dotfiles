;; my-web --- -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(require 'eww)
(defun my/eww-save-binary-file () ".")
(define-key eww-mode-map (kbd "M-RET") nil)

(let* ((filename nil)
       (dir-or-pwd (lambda ()
                     (if (file-writable-p eww-download-directory)
                         eww-download-directory default-directory)))
       (o (lambda (url)
            (let* ((o (url-path-and-query (url-generic-parse-url url)))
                   (o (directory-file-name (car o))))
              (eww-decode-url-file-name (file-name-nondirectory o))))))
  (let ((aux (lambda (args)
               (let ((status (car args)) (url (cadr args)) (rest (cddr args)))
                 (let* ((cd (assoc "content-disposition" (eww-parse-headers)))
                        (cd (when cd (cdr cd))))
                   (setq filename nil)
                   (when (and cd (string-match "filename=\"\\(.*\\)\"" cd))
                     (setq filename (match-string 1 cd))))
                 (let ((redirect (plist-get status :redirect)))
                   (unless filename
                     (setq filename (funcall o (if redirect redirect url))))
                   `(,status ,(if redirect redirect url) ,@rest))))))
    (advice-add 'eww-render :filter-args aux)
    (advice-add 'eww-download-callback :filter-args aux))
  (setq eww-download-directory "~")
  (advice-add 'eww-make-unique-file-name :filter-args
              (lambda (_args)
                `(,(if (zerop (length filename)) "index.html" filename)
                  ,(funcall dir-or-pwd))))
  (advice-add 'my/eww-save-binary-file :override
              (lambda ()
                (goto-char (point-min))
                (unwind-protect
                    (let ((file (read-file-name
                                 (format "%s to save as: " filename)
                                 (funcall dir-or-pwd) filename))
                          (require-final-newline nil))
                      (write-region (point-min) (point-max) file))
                  (kill-buffer (current-buffer)))))
  (setq mailcap-user-mime-data
        '(((viewer . my/eww-save-binary-file) (type . "application/.+")))))

(provide 'my-web)
;;; my-web.el ends here
