;; my-image --- -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(defvar-local my/image nil)
(put 'my/image 'risky-local-variable t)

(defun my/image (w h)
  "Show image pixels which W and H compose the pixel of a character."
  (if (not (derived-mode-p 'image-mode)) ""
    (letrec ((my-gcd (lambda (lhs rhs)
                       (if (zerop rhs) lhs (funcall my-gcd rhs (% lhs rhs))))))
      (let* ((wh (image-size my/image))
             (wh (cons (* (car wh) w) (* (cdr wh) h)))
             (wh (cons (round (car wh)) (round (cdr wh))))
             (gcd (funcall my-gcd (car wh) (cdr wh))))
        (format " [%sx%s %s:%s]"
                (car wh) (cdr wh) (/ (car wh) gcd) (/ (cdr wh) gcd))))))

(require 'image-mode)
(let (frame wh)
  (add-hook 'image-mode-hook
            (lambda ()
              (let ((buffer (current-buffer)))
                (image-transform-set-scale 1)
                (setq-local my/image (image-get-display-property))
                (image-transform-fit-both)
                (unless (and frame (frame-live-p frame))
                  (setq frame (make-frame '((alpha . (100 . 100))))))
                (unless wh
                  (setq wh (let ((buffer (get-buffer-create "*tmp*")))
                             (switch-to-buffer buffer)
                             (erase-buffer)
                             (insert "a")
                             (prog1 (window-text-pixel-size)
                               (kill-buffer buffer)))))
                (select-frame frame)
                (switch-to-buffer buffer)
                (add-to-list 'mode-line-misc-info
                             `(:eval (my/image ,(car wh) ,(cdr wh))))))))

(provide 'my-image)
;;; my-image.el ends here
