;; my-interaction ---  -*- lexical-binding: nil -*-

;;; Commentary:

;;; Code:

(require 'icomplete)
(fido-mode t)
(setq-default completion-styles '(partial-completion flex))
(add-hook 'icomplete-minibuffer-setup-hook
          (lambda () (setq-local completion-styles '(partial-completion flex)
                                 icomplete-show-matches-on-no-input nil)))

(require 'ido)
(ido-mode t)
(add-to-list 'ido-ignore-buffers "\\`\\*")
(setq ido-enable-flex-matching t ido-show-dot-for-dired t
      ido-auto-merge-work-directories-length -1
      search-exit-option 'edit search-whitespace-regexp "[^\r\n]*")
(advice-add 'ido-read-internal :after
            (lambda (_i _p hist &rest _rest)
              (when (sequencep (symbol-value hist))
                (set hist (delete-dups (symbol-value hist))))))
(advice-add 'ido-trace :after
            (lambda (p &optional s _)
              (cond ((equal "read-from-minibuffer" p) (setq ido-text-init s))
                    ((equal "cd" p)
                     (when (active-minibuffer-window)
                       (delete-region (minibuffer-prompt-end) (point-max))))
                    ((and (equal "\n_EXIT_" p) (null s))
                     (setq ido-text-init nil)))))

(require 'cl-lib)
(cl-macrolet ((my-ido
               (point fallback &body body)
               `(lambda (n)
                  (interactive "p")
                  (if (not (= (funcall ,point) (point))) (funcall ,fallback n)
                    ,@body))))
  (let ((ido-wide-find-file-or-forward-word
         (my-ido 'point-max 'forward-word
                 (ido-wide-find-file
                  (substring (buffer-string) (minibuffer-prompt-end)))))
        (ido-up-directory-or-backward-word
         (my-ido 'minibuffer-prompt-end 'backward-word (ido-up-directory)))
        (ido-wide-find-dir-or-kill-word
         (my-ido 'point-max 'kill-word
                 (ido-wide-find-dir
                  (substring (buffer-string) (minibuffer-prompt-end)))))
        (ido-delete-file-or-kill-line
         (my-ido 'point-max 'kill-line (ido-delete-file-at-head)))
        (ido-kill-buffer-or-kill-line
         (my-ido 'point-max 'kill-line (ido-kill-buffer-at-head)))
        (fido-kill-buffer-or-kill-line
         (my-ido 'point-max 'kill-line (icomplete-fido-kill))))
    (dolist (k-m `((,ido-common-completion-map
                    ,minibuffer-local-map
                    ("<tab>" ido-complete) ("<space>" ido-complete-space)
                    ("M-j" ido-select-text) ("C-m" ido-exit-minibuffer)
                    ("C-n" ido-next-match) ("C-p" ido-prev-match)
                    ("C-o" ido-fallback-command))
                   (,ido-file-dir-completion-map
                    ,ido-common-completion-map
                    ("M-n" ido-next-match-dir) ("M-p" ido-prev-match-dir)
                    (backward-kill-word ido-delete-backward-word-updir)
                    (delete-backward-char ido-delete-backward-updir)
                    ("M-f" ,ido-wide-find-file-or-forward-word)
                    ("M-b" ,ido-up-directory-or-backward-word)
                    ("M-d" ,ido-wide-find-dir-or-kill-word))
                   (,ido-file-completion-map
                    ,ido-file-dir-completion-map
                    ("C-k" ,ido-delete-file-or-kill-line))
                   (,ido-buffer-completion-map
                    ,ido-common-completion-map
                    ("C-k" ,ido-kill-buffer-or-kill-line))
                   (,icomplete-fido-mode-map
                    nil ("<tab>" icomplete-force-complete)
                    ("RET" icomplete-force-complete-and-exit)
                    ("C-k" ,fido-kill-buffer-or-kill-line)
                    ("C-p" icomplete-backward-completions)
                    ("C-n" icomplete-forward-completions)
                    ("DEL" icomplete-fido-backward-updir)
                    ("M-j" icomplete-fido-exit))))
      (setcdr (car k-m) nil)
      (when (cadr k-m) (set-keymap-parent (car k-m) (cadr k-m)))
      (dolist (k (cddr k-m))
        (define-key (car k-m)
          (if (symbolp (car k)) (vector 'remap (car k)) (kbd (car k)))
          (cadr k))))))

(provide 'my-interaction)
;;; my-interaction.el ends here
