;; my-dired --- -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(defvar my/dired-marked)

(require 'dired-aux)
(advice-add 'dired-insert-set-properties :after
            (lambda (b e)
              (save-excursion
                (goto-char b)
                (while (< (point) e)
                  (when (dired-move-to-filename)
                    (set-text-properties
                     (point)
                     (progn (dired-move-to-end-of-filename) (point))
                     '(mouse-face highlight dired-filename t help-echo
                                  "double-mouse-1: open file or directory")))
                  (forward-line)))))

(require 'popup)
(add-hook 'dired-mode-hook
          (lambda ()
            (setcdr dired-mode-map nil)
            (put 'dired-find-alternate-file 'disabled nil)
            (let* ((dfy (lambda (d)
                          (interactive "P")
                          (let ((buffer-file-name (dired-get-filename)))
                            (funcall (global-key-binding (kbd "C-]")) d))))
                   (first (lambda ()
                            (interactive)
                            (goto-char (point-min))
                            (while (not (dired-get-filename nil t))
                              (dired-next-line 1))))
                   (last (lambda ()
                           (interactive)
                           (goto-char (point-max))
                           (dired-next-line -1)))
                   (prev (lambda (n)
                           (interactive "p")
                           (dired-next-line (- (or n 1)))
                           (unless (dired-get-filename nil t) (funcall first))))
                   (next (lambda (n)
                           (interactive "p")
                           (dired-next-line (or n 1))
                           (unless (dired-get-filename nil t) (funcall last))))
                   (mall (lambda ()
                           (interactive)
                           (dired-unmark-all-marks)
                           (dired-toggle-marks)))
                   (findf (lambda ()
                            (interactive)
                            (let ((c (save-excursion (dired-move-to-filename)))
                                  (d (save-excursion
                                       (re-search-backward dired-re-dir nil t)
                                       (dired-move-to-filename))))
                              (if (= c d) (dired-find-alternate-file)
                                (dired-find-file)))))
                   (updir (lambda () (interactive) (find-alternate-file "..")))
                   (fpos (lambda ()
                           (let ((b (save-excursion
                                      (dired-move-to-filename nil))))
                             (when (and b (< (point) b)) (goto-char b)))))
                   (revert (lambda (dir)
                             (and (not (file-remote-p (expand-file-name dir)))
                                  (dired-directory-changed-p dir))))
                   (dorev (lambda (&rest _)
                            (when (funcall revert default-directory)
                              (revert-buffer))))
                   (doop (lambda (op src &rest files)
                           (let ((dst default-directory))
                             (dolist (f files)
                               (let ((s (expand-file-name f src))
                                     (d (expand-file-name f dst)))
                                 (dlet (overwrite-backup-query
                                        (dired-overwrite-confirmed
                                         (file-exists-p d)))
                                   (cl-letf (((symbol-function 'yes-or-no-p)
                                              'y-or-n-p))
                                     (funcall op s d 1)))))
                             (when (funcall revert dst) (revert-buffer)))))
                   (notf (lambda (f) (not (member f '("." "..")))))
                   (f (lambda (op)
                        (setq my/dired-marked
                              `(,op ,default-directory
                                    ,@(dired-get-marked-files t nil notf)))))
                   (cp (lambda () (interactive) (funcall f 'dired-copy-file)))
                   (mv (lambda () (interactive) (funcall f 'dired-rename-file)))
                   (pst (lambda ()
                          (interactive)
                          (when my/dired-marked (apply doop my/dired-marked))))
                   (del (lambda ()
                          (interactive)
                          (let ((c (current-buffer))
                                (f (dired-get-marked-files t nil notf))
                                (r (lambda ()
                                     (when (funcall revert default-directory)
                                       (revert-buffer)))))
                            (when f
                              (if (file-remote-p default-directory)
                                  (when (y-or-n-p (format "Delete %s? "
                                                          (string-join f " ")))
                                    (dolist (file f) (delete-file file))
                                    (funcall r))
                                (make-process
                                 :name "trash" :connection-type 'pipe :noquery t
                                 :command `("gio" "trash" ,@f) :buffer nil
                                 :sentinel
                                 (lambda (proc _)
                                   (when (eq 'exit (process-status proc))
                                     (with-current-buffer c (funcall r))))))))))
                   (rn (lambda (fn)
                         (interactive
                          (let ((fn (dired-get-filename t)))
                            (list (read-string "Rename file: " fn nil fn))))
                         (dlet ((use-short-answers t))
                           (rename-file (dired-get-filename t) fn 1))
                         (dired-update-file-line fn)))
                   (tch (lambda (fn)
                          (interactive
                           (let* ((fn (dired-get-filename t))
                                  (prompt (concat "Touch file (" fn "): ")))
                             (list (read-string prompt nil nil fn))))
                          (if (not (file-exists-p fn))
                              (dired-create-empty-file fn)
                            (set-file-times fn)
                            (dired-update-file-line fn))))
                   (open (lambda ()
                           (interactive)
                           (if (file-remote-p default-directory)
                               (error "Cannot open in remote") ;; pkexec?
                             (let ((fn (dired-get-filename)))
                               (make-process
                                :name "mime" :connection-type 'pipe :noquery t
                                :buffer (generate-new-buffer "*mime*")
                                :command `("xdg-mime" "query" "filetype" ,fn)
                                :sentinel
                                (lambda (proc _)
                                  (when (eq 'exit (process-status proc))
                                    (let* ((mime
                                            (with-current-buffer
                                                (process-buffer proc)
                                              (prog1 (string-trim
                                                      (buffer-string))
                                                (kill-buffer))))
                                           (apps (xdg-mime-apps mime))
                                           (f (lambda (f)
                                                (let ((n (file-name-base f)))
                                                  (popup-make-item
                                                   n :value f))))
                                           (f (if (not (cdr apps)) (car apps)
                                                (popup-menu* (mapcar f apps)))))
                                      (call-process
                                       "gio" nil 0 nil "launch" f fn)))))))))
                   (m1 (lambda (event)
                         (interactive "e")
                         (goto-char (posn-point (event-start event)))
                         (let* ((b (generate-new-buffer "*file*"))
                                (f (dired-get-filename t t))
                                (p (start-file-process "file" b "file" "-L" f))
                                (curr (current-buffer)))
                           (set-process-sentinel
                            (prog1 p (set-process-query-on-exit-flag p nil))
                            (lambda (proc _)
                              (when (eq 'exit (process-status proc))
                                (let ((type (with-current-buffer b
                                              (string-trim (buffer-string))))
                                      (p "\\`.+: \\([^,]+\\)\\(,.+\\)?\\'"))
                                  (kill-buffer b)
                                  (with-current-buffer curr
                                    (let ((str (when (string-match p type)
                                                 (match-string 1 type))))
                                      (cond ((string-suffix-p "directory" str)
                                             (find-alternate-file f))
                                            ((or (string-suffix-p "script" str)
                                                 (string-suffix-p "text" str)
                                                 (and (string-suffix-p
                                                       "compressed data" str)
                                                      (not (string-match-p
                                                            "\\.tar\\." f))))
                                             (find-file f))
                                            (t (funcall open))))))))))))
                   (m2 (lambda (event)
                         (interactive "e")
                         (goto-char (posn-point (event-start event)))
                         (let ((f (dired-get-filename)))
                           (if (dired-file-marker f) (dired-unmark nil)
                             (dired-mark nil)))
                         (forward-line -1))))
              (dolist (k `(("<" dired-prev-dirline) ("M-<" ,first) ("M->" ,last)
                           (">" dired-next-dirline) ("." dired-clean-directory)
                           ("~" dired-flag-backup-files) (previous-line ,prev)
                           ("w" ,cp) ("v" ,mv) ("y" ,pst) ("n" ,rn) ("t" ,tch)
                           ("x" ,del) ("o" ,open) ("^" ,updir) ("C-]" ,dfy)
                           ("m" dired-mark) ("M" ,mall) ("u" dired-unmark)
                           ("U" dired-unmark-all-marks) (next-line ,next)
                           ("d" dired-create-directory) ("RET" ,findf)
                           ("<double-mouse-1>" ,m1) ("<mouse-2>" ,m2)))
                (define-key dired-mode-map
                  (if (symbolp (car k)) (vector 'remap (car k)) (kbd (car k)))
                  (cadr k)))
              (setq dired-auto-revert-buffer revert dired-backup-overwrite t
                    dired-deletion-confirmer 'y-or-n-p
                    dired-listing-switches "-hal")
              (add-hook 'window-buffer-change-functions dorev nil t)
              (add-hook 'post-command-hook fpos nil t))))

(require 'woman)
(advice-add 'woman-dired-define-keys :override (lambda ()))

(require 'xdg)
(advice-add
 'xdg-mime-apps-files :filter-return
 (lambda (fs)
   (letrec ((f (lambda (fs)
                 (let ((c "applications/mimeinfo.cache"))
                   (if (string-suffix-p "mimeapps.list" (car fs))
                       (cons (car fs) (and (cdr fs) (funcall f (cdr fs))))
                     (cons (expand-file-name c (xdg-data-home)) fs))))))
     (funcall f fs))))

(define-minor-mode my/dired-trash-mode "Trash Can." :interactive nil :keymap
  (let ((map (make-sparse-keymap)))
    (dolist (key '("~" "w" "v" "y" "n" "t" "d" "^"))
      (define-key map key 'self-insert-command))
    (let ((op (lambda (op)
                (dolist (f (dired-get-marked-files t))
                  (let* ((i (concat "../info/" f ".trashinfo"))
                         (i (expand-file-name i))
                         (f (expand-file-name f)))
                    (funcall op f i)))
                (when (funcall dired-auto-revert-buffer default-directory)
                  (revert-buffer))))
          (del (lambda (f i)
                 (let ((p (concat "Delete " (file-name-nondirectory f) "? ")))
                   (when (y-or-n-p p) (delete-file f) (delete-file i)))))
          (rev (lambda (f i)
                 (let* ((s (with-temp-buffer
                             (insert-file-contents i)
                             (buffer-string))))
                   (when (string-match "^Path=\\(.+\\)$" s)
                     (let ((raw (url-unhex-string (match-string 1 s))))
                       (rename-file f (decode-coding-string raw 'utf-8)))
                     (delete-file i))))))
      (define-key map (kbd "x") (lambda () (interactive) (funcall op del)))
      (define-key map (kbd "r") (lambda () (interactive) (funcall op rev))))
    map)
  (let ((inhibit-message t)) (rename-buffer "trash:///" t) (dired-omit-mode)))

(let ((trash (lambda (&rest _)
               (when (string-match-p
                      (concat "/\\(\\.\\)?Trash\\(-" (int-to-string (user-uid))
                              "\\)?/files/")
                      default-directory)
                 (my/dired-trash-mode 1)))))
  (add-hook 'dired-mode-hook trash)
  (add-function
   :after after-focus-change-function
   (lambda ()
     (dolist (f (frame-list))
       (with-selected-frame f
         (when (frame-focus-state)
           (dolist (w (window-list))
             (with-current-buffer (window-buffer w)
               (when (and (derived-mode-p 'dired-mode)
                          (funcall dired-auto-revert-buffer default-directory))
                 (revert-buffer))))))))))

(provide 'my-dired)
;;; my-dired.el ends here
