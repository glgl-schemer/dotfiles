;; my-terminal --- -*- lexical-binding: nil -*-

;;; Commentary:

;;; Code:

(require 'tramp-sh)
(let ((bash (assoc "/bash\\'" tramp-sh-extra-args)))
  (if bash (setcdr bash (concat (cdr bash) " -l -i"))
    (add-to-list 'tramp-sh-extra-args '("/bash\\'" . "-l -i")))
  (add-to-list 'debug-ignored-errors 'remote-file-error)
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path)
  (add-to-list 'tramp-remote-process-environment "HISTIGNORE='*tramp*'")
  (remove-hook 'kill-buffer-hook #'tramp-flush-file-function)
  (setq-default auth-source-save-behavior nil password-cache-expiry 3600
                eshell-prefer-lisp-functions t eshell-prefer-lisp-variables t
                epg-pinentry-mode 'loopback tramp-histfile-override nil))

(require 'vterm)
(setq vterm-term-environment-variable "xterm-16color" vterm-max-scrollback 10000
      vterm-eval-cmds nil vterm-timer-delay nil vterm-always-compile-module t
      vterm-buffer-name-string "%s" vterm-clear-scrollback-when-clearing t)
(dolist (k '("C-c" "C-g" "C-h" "C-l" "C-u" "C-v" "C-y" "C-M-y"
             "C-/" "C-'" "C-\"" "C-_" "C--" "C-?"))
  (define-key vterm-mode-map (kbd k) 'vterm--self-insert))
(dolist (cf '(("<C-delete>" . vterm-send-C-d) ("<C-backspace>" . vterm-send-C-h)
              ("C-x M-+" . universal-argument) ("C-\\" . toggle-input-method)
              ("C-x M-:" . eval-expression) ("C-x C-y" . vterm-yank)
              ("C-x M-\"" . help-command) ("C-M-x" . vterm-send-C-x)
              ("C-x M-x" . execute-extended-command)))
  (define-key vterm-mode-map (kbd (car cf)) (cdr cf)))
(dolist
    (fbf '((vterm-color-bright-black vterm-color-black "#222222" "#000000")
           (vterm-color-bright-red vterm-color-red "#a65544" "#862222")
           (vterm-color-bright-green vterm-color-green "#86c686" "#226622")
           (vterm-color-bright-yellow vterm-color-yellow "#c6c686" "#666600")
           (vterm-color-bright-blue vterm-color-blue "#6686a6" "#002286")
           (vterm-color-bright-magenta vterm-color-magenta "#a686c6" "#662286")
           (vterm-color-bright-cyan vterm-color-cyan "#86c6c6" "#226666")
           (vterm-color-bright-white vterm-color-white "#c6c6c6" "#868686")))
  (set-face-background (car fbf) (caddr fbf))
  (set-face-foreground (car fbf) (caddr fbf))
  (set-face-background (cadr fbf) (cadddr fbf))
  (set-face-foreground (cadr fbf) (cadddr fbf)))

(defvar-local my/vterm-sh nil)

(cl-macrolet
    ((M- (&rest c)
         `(progn
            ,@(mapcar (lambda (c)
                        `(define-key vterm-mode-map (kbd ,(concat "M-" c))
                           (lambda () (interactive) (vterm-send-key ,c nil t))))
                      c))))
  (M- "b" "c" "d" "f" "l" "n" "o" "p" "u" "v" "w" "x" "y" ":" "," "." "?" "]"
      "<" ">" "+" "-" "_" "\"")) ;; ESC-

(let ((vterm-send-C-SP (lambda () (interactive) (vterm-send-key " " nil nil t)))
      (vterm-send-C-M-x (lambda () (interactive) (vterm-send-key "x" nil t t)))
      (vterm-yank-menu
       (lambda ()
         (interactive)
         (vterm-insert (funcall (global-key-binding (kbd "C-x y"))))))
      (tmux-copy-mode
       (lambda ()
         (interactive)
         (if (not (string-prefix-p "tmux " my/vterm-sh)) (vterm-copy-mode)
           (vterm-send-key "]" nil t)
           (vterm-send-key "[")))))
  (define-key vterm-mode-map (kbd "C-x y") vterm-yank-menu)
  (define-key vterm-mode-map (kbd "C-x C-z") tmux-copy-mode)
  (define-key vterm-mode-map (kbd "C-x C-M-x") vterm-send-C-M-x)
  (define-key vterm-mode-map (kbd "C-@") vterm-send-C-SP)
  (define-key vterm-mode-map (kbd "C-`") vterm-send-C-SP)
  (define-key vterm-mode-map (kbd "M-RET") vterm-send-C-SP)
  (define-key vterm-copy-mode-map (kbd "C-c") 'vterm-copy-mode-done))

(let ((my-vterm
       (lambda (&optional keep)
         (interactive "P")
         (with-current-buffer (generate-new-buffer "vterm")
           (unless (derived-mode-p 'vterm-mode)
             (let* ((name (concat (buffer-name) (number-to-string (emacs-pid))))
                    (conf "~/.config/tmux/tmux.conf")
                    (vterm-shell
                     (if (and (executable-find "tmux" t)
                              (if (string-match
                                   "\\`/\\([^/:]+:[^/:]*|\\)*[^/:]+:[^/:]*:"
                                   default-directory)
                                  (file-readable-p
                                   (concat
                                    (match-string 0 default-directory) conf))
                                (file-readable-p conf)))
                         (concat "tmux -f " conf
                                 (unless keep (concat " new -s \"" name "\"")))
                       vterm-shell)))
               (vterm-mode))
             (set-process-query-on-exit-flag
              (get-buffer-process (current-buffer)) nil)
             (unless keep
               (add-hook 'kill-buffer-hook
                         (lambda ()
                           (let ((name (concat (buffer-name)
                                               (number-to-string (emacs-pid)))))
                             (make-process
                              :command `("tmux" "kill-session" "-t" ,name)
                              :file-handler t :name "tmux" :noquery t)))
                         nil t))
             (switch-to-buffer (current-buffer))))))
      (toggle-buffer-read-only
       (lambda (&rest _rest)
         (setq buffer-read-only (not buffer-read-only)))))
  (advice-add 'vterm :override my-vterm)
  (add-hook 'vterm-mode-hook (lambda () (setq-local my/vterm-sh vterm-shell)))
  (add-hook 'vterm-mode-hook toggle-buffer-read-only)
  (add-hook 'vterm-copy-mode-hook toggle-buffer-read-only))

(provide 'my-terminal)
;;; my-terminal.el ends here
