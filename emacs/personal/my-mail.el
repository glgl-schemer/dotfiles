;; my-mail --- -*- lexical-binding: nil -*-

;;; Commentary:

;;; Code:

(require 'vterm)
(add-to-list 'auto-mode-alist '("/mutt[^/]+\\'" . mail-mode))

(defun my/mutt (proxychains mutt-folder)
  "MUTT-FOLDER should contents `.muttrc'.
One folder one instance.  If PROXYCHAINS is not nil, use `proxychians'."
  (interactive "P\nDChoose mutt folder: ")
  (unless (executable-find "mutt" t) (error "Mutt not found!"))
  (setq mutt-folder (directory-file-name mutt-folder))
    (let ((f (concat mutt-folder "/.muttrc")))
      (unless (file-readable-p f)
        (error "Mutt configuration file not exist!"))
      (let ((tbuffer (get-buffer-create
                      (concat "mutt: " (expand-file-name mutt-folder t)))))
        (unless (vterm-check-proc tbuffer)
          (with-current-buffer tbuffer
            (cd mutt-folder)
            (let ((vterm-shell
                   (concat (if (and proxychains
                                    (executable-find "proxychains") t)
                               "proxychains ")
                           "mutt -F " f)))
              (vterm-mode))))
        (switch-to-buffer tbuffer))))

(define-minor-mode my/mutt-render-mode
  "." :require 'shr
  (when my/mutt-render-mode
    (let ((inhibit-message t) (case-fold-search t))
      (unwind-protect
          (let ((pattern '("[\r\n ]*<\\(!doctype \\)?html\\(>\\| \\)"
                           "[\r\n ]*<meta\\(>\\| \\)")))
            (search-forward-regexp "^\n" nil t 2)
            (letrec ((look (lambda (seq)
                             (when seq (or (looking-at-p (car seq))
                                           (funcall look (cdr seq)))))))
              (when (funcall look pattern)
                (advice-add 'shr-image-fetched :after
                            (lambda (_ buffer &rest _rest)
                              (with-current-buffer buffer
                                (when my/mutt-render-mode
                                  (let ((inhibit-message t)) (save-buffer))))))
                (shr-render-region (point) (point-max)))))
        (goto-char (point-min))
        (save-buffer)))))

(provide 'my-mail)
;;; my-mail.el ends here
