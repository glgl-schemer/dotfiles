;; my-im --- -*- lexical-binding: nil -*-

;;; Commentary:

;;; Code:

(require 'rime)
(setq default-input-method "rime" rime-show-candidate 'minibuffer
      rime-show-preedit t rime-user-data-dir "~/.config/fcitx5/rime")

(advice-add 'rime--load-dynamic-module :around
            (lambda (func) (let ((inhibit-message t)) (funcall func))))

(advice-add 'rime--minibuffer-message :override
            (lambda (string)
              (message nil)
              (unless (string-blank-p string)
                (let ((inhibit-quit t) point-1)
                  (save-excursion
                    (insert (concat " | " string))
                    (setq point-1 (point)))
                  (sit-for 1000000)
                  (delete-region (point) point-1)
                  (when quit-flag
                    (setq quit-flag nil unread-command-events '(7)))))))

(provide 'my-rime)
;;; my-rime.el ends here
