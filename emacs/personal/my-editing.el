;; my-editing --- -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(dolist (hook '(prog-mode-hook text-mode-hook))
  (add-hook hook 'display-fill-column-indicator-mode)
  (add-hook hook 'display-line-numbers-mode))

(let ((ins (lambda () (setq-local tab-always-indent (not tab-always-indent))
             (let ((inhibit-message t))
               (call-interactively 'delete-selection-mode))))
      (mv-dir (lambda () (unless (file-directory-p default-directory)
                           (setq default-directory temporary-file-directory))))
      (untabify (lambda () (unless (or (derived-mode-p 'makefile-mode)
                                       (eq major-mode 'c-mode)
                                       (eq major-mode 'c++-mode))
                             (untabify (point-min) (point-max)))))
      (truncate (lambda () (setq truncate-lines t) (force-mode-line-update))))
  (add-hook 'before-save-hook 'delete-trailing-whitespace)
  (add-hook 'before-save-hook untabify)
  (add-hook 'kill-buffer-hook mv-dir)
  (add-hook 'overwrite-mode-hook ins)
  (add-hook 'prog-mode-hook truncate)
  (add-hook 'text-mode-hook 'visual-line-mode)
  (add-hook 'visual-line-mode-hook (lambda () (setq-local word-wrap nil))))

(setq-default fill-column 80)
(setq column-number-mode t focus-follows-mouse t inhibit-startup-screen t
      auto-save-default nil auto-save-list-file-prefix nil indent-tabs-mode nil
      make-backup-files nil resize-mini-windows nil custom-file null-device
      save-interprogram-paste-before-kill t kill-do-not-save-duplicates t
      vc-handled-backends nil)

(provide 'my-editing)
;;; my-editing.el ends here
