;;; stardict.el --- stardict dictionary library

;; Copyright (C) 2010 Changyuan Yu
;; Copyright (C) 2023 glgl-schemer

;; Author: Changyuan Yu <rei.vzy@gmail.com>
;; Created: 2010-11-06
;; Version: 0.2
;; Keywords: stardict

;; This file is *NOT* part of GNU Emacs

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; A copy of the GNU General Public License can be obtained from this
;; program's author (send electronic mail to andyetitmoves@gmail.com)
;; or from the Free Software Foundation, Inc.,
;; 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

;;; Commentary:
;;

;; Example:
;;
;; (require 'stardict)
;; (setq dict
;;       (stardict-open "~/.stardict/dic/stardict-lazyworm-ec-2.4.2"
;;                      "lazyworm-ec"))
;; (stardict-word-exist-p dict "apple")
;; (stardict-lookup dict "apple")

;; (stardict-open-dict-file dict)
;; (mapcar (lambda (x) (stardict-lookup dict x)) (make-list 1000 "apple"))

;;; Code:

(require 'cl-lib)
(require 'subr-x)

(defun stardict-str2int (str)
  "Convert string `STR' to integer.
\x21\x22 => 0x2122"
  (cl-reduce (lambda (sum c) (+ (* sum #x100) (mod c #x100)))
             (append str nil)
             :initial-value 0))

(defun stardict-open (dir name &optional nocache)
  "Open stardict dictionary in directory `DIR' with name `NAME'.
When `NOCACHE' is not nil, don't load from cache and save to cache.
The return is used as `DICT' argument in other functions."
  (if nocache (stardict-open-1 dir name)
    (let ((cache (expand-file-name (concat name ".idx.emacs.gz") dir)))
      (if (file-readable-p cache)
          (with-temp-buffer
            (with-auto-compression-mode (insert-file-contents cache))
            (read (current-buffer)))
        (with-temp-buffer
          (prog1 (prin1 (stardict-open-1 dir name) (current-buffer))
            (with-auto-compression-mode (write-region nil nil cache))))))))

(defun stardict-open-1 (dir name)
  "Internal function used by `stardict-open'.
`DIR' is dictionary location, `NAME' is dictionary name."
  (let* ((ifo  (expand-file-name (concat name ".ifo") dir))
         (idx  (expand-file-name (concat name ".idx") dir))
         (dict (expand-file-name (concat name ".dict") dir))
         (idx  (if (file-readable-p idx) idx (concat idx ".gz")))
         (dict (if (file-readable-p dict) dict (concat dict ".gz")))
         (ifo-ht (make-hash-table :test 'equal))
         (idx-ht (make-hash-table :test 'equal))
         (idx-offset-bytes 4)
         (word-count 0))
    (unless (cl-every 'file-readable-p `(,ifo ,idx ,dict))
      (error "Dict %s not found in %s" name dir))
    ;; get info
    (with-temp-buffer
      (insert-file-contents ifo)
      (goto-char (point-min))
      (while (re-search-forward "^\\([a-zA-Z]+\\)=\\(.*\\)$" nil t)
        (puthash (match-string 1) (match-string 2) ifo-ht)))
    (when (gethash "idxoffsetbits" ifo-ht)
      (setq idx-offset-bytes
            (/ (string-to-number (gethash "idxoffsetbits" ifo-ht)) 8)))
    (setq word-count (string-to-number (gethash "wordcount" ifo-ht)))
    ;; get index
    (with-auto-compression-mode
      (with-temp-buffer
        (insert-file-contents idx)
        (goto-char (point-min))
        (let ((rpt (make-progress-reporter "read index: " 0 (1- word-count))))
          (dotimes (i word-count)
            (progress-reporter-update rpt i)
            (re-search-forward "\\([^\x00]+?\\)\x00" nil t)
            (let* ((p (point))
                   (word (decode-coding-string
                          (encode-coding-string (match-string 1) 'no-conversion)
                          'utf-8))
                   (offset (stardict-str2int
                            (buffer-substring-no-properties
                             p (+ p idx-offset-bytes))))
                   (size (stardict-str2int
                          (buffer-substring-no-properties
                           (+ p idx-offset-bytes) (+ p idx-offset-bytes 4)))))
              (forward-char (+ idx-offset-bytes 4))
              (puthash word (cons offset size) idx-ht))))
        (unless (hash-table-empty-p idx-ht)
          (list ifo-ht idx-ht dict))))))

(defun stardict-word-exist-p (dict word)
  "Checkout whether `WORD' existed in `DICT'."
  (gethash word (nth 1 dict)))

(defvar stardict-file-buffer-alist nil
  "Record the dictionary file and its buffer.")

(defun stardict-lookup (dict word)
  "Lookup `WORD' in `DICT', return nil when not found."
  (let ((info (stardict-word-exist-p dict word)))
    (when info
      (let* ((file (nth 2 dict))
             ;; find any opened dict file
             (buffer
              (cl-some (lambda (buf)
                         (let ((al stardict-file-buffer-alist))
                           (when (equal buf (alist-get file al nil nil 'equal))
                             buf)))
                       (buffer-list)))
             (offset (car info))
             (size (cdr info)))
        (if buffer
            (with-current-buffer buffer
              (buffer-substring-no-properties
               (byte-to-position (1+ offset))
               (byte-to-position (+ 1 offset size))))
          (with-temp-buffer
            (with-auto-compression-mode
              (insert-file-contents file nil offset (+ offset size)))
            (buffer-string)))))))

(defun stardict-open-dict-file (dict)
  "Open dict file of `DICT' in Emacs to speed up word lookup.
You should close the dict file yourself."
  (with-current-buffer (get-buffer-create (concat "*" (nth 2 dict) "*"))
    (unless buffer-read-only
      (setq stardict-file-buffer-alist
            `((,(nth 2 dict) . ,(current-buffer)) ,@stardict-file-buffer-alist))
      (erase-buffer)
      (with-auto-compression-mode (insert-file-contents (nth 2 dict)))
      (setq buffer-read-only t))))

(defvar stardict-file-directory-plist
  (mapcan (lambda (dict) (list dict (concat user-emacs-directory "dict")))
          '("AHD4" "CALD3" "MHLD" "OCD2" "WNI3"))
  "A list of directory files and their locations.")

(defun stardict-open-directories (plist)
  "Open directories from PLIST."
  (when (and (consp plist) (consp (cdr plist)))
    (let ((dict (stardict-open (cadr plist) (car plist))))
      (if dict (cons dict (stardict-open-directories (cddr plist)))
        (stardict-open-directories (cddr plist))))))

(defun stardict-lookup-directories (word)
  "Lookup WORD in directories specified by `stardict-file-directory-plist'."
  (let ((dicts (stardict-open-directories stardict-file-directory-plist))
        (buffer (current-buffer)))
    (with-current-buffer (get-buffer-create "*stardict*")
      (setq buffer-read-only nil)
      (erase-buffer)
      (dolist (dict dicts)
        (when (stardict-word-exist-p dict word)
          (stardict-open-dict-file dict)
          (let ((dname (if (string-suffix-p ".gz" (nth 2 dict))
                          (string-remove-suffix ".gz" (nth 2 dict))
                        (nth 2 dict)))
                (shr-inhibit-images t)
                (shr-use-fonts nil)
                (point (point)))
            (insert "<title>" (file-name-base dname) "</title>")
            (insert (stardict-lookup dict word))
            (replace-regexp-in-region "<hr[^>]*>" "<br/>" point (point-max))
            (shr-render-region point (point-max))
            (goto-char (point-max))
            (insert ?\n))))
      (setq buffer-read-only t)
      (unless (string-blank-p (buffer-string))
        (goto-char (point-min))
        (switch-to-buffer-other-window (current-buffer))
        (switch-to-buffer-other-window buffer)))))

(defun stardict-lookup-at-point (word)
  "Lookup WORD at point."
  (interactive
   (let* ((word
           (if mark-active
               (buffer-substring-no-properties (region-beginning) (region-end))
             (substring-no-properties (or (thing-at-point 'word) ""))))
          (word (unless (string-blank-p word) (downcase word))))
     (list (read-string "Describe word: " word nil word))))
  (stardict-lookup-directories word))

(defun stardict-lookup-current-kill (word)
  "Lookup WORD in the clipboard."
  (interactive
   (let* ((word (substring-no-properties (or (current-kill 0) "")))
          (word (unless (string-blank-p word) (downcase word))))
     (list (read-string "Describe killed word: " word nil word))))
  (stardict-lookup-directories word))

(define-key help-map (kbd "w") 'stardict-lookup-at-point)

(define-key help-map (kbd "y") 'stardict-lookup-current-kill)

(provide 'stardict)

;;; stardict.el ends here
