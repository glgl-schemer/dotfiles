;; init --- -*- lexical-binding: t -*-

;;; Commentary:

;;; Code:

(let* ((file-name-handler-alist-original file-name-handler-alist)
       (gc-cons-threshold-original gc-cons-threshold)
       (reset (lambda ()
                (setq file-name-handler-alist file-name-handler-alist-original)
                (setq gc-cons-threshold gc-cons-threshold-original))))
  (setq gc-cons-threshold 500000000)
  (setq file-name-handler-alist nil)
  (add-hook 'window-setup-hook (lambda () (run-with-idle-timer 2 nil reset))))

(add-to-list 'load-path (concat user-emacs-directory "personal"))

(setq native-comp-jit-compilation nil native-comp-enable-subr-trampolines nil)

(require 'my-package)

(setq warning-minimum-level :emergency)

(require 'my-appearance)
(require 'my-dired)
(require 'my-editing)
(require 'my-image)
(require 'my-interaction)
(require 'my-key)
(require 'my-mail)
(require 'my-org)
(require 'my-programming)
(require 'my-rime)
(require 'my-terminal)
(require 'my-typo)
(require 'my-web)
(require 'stardict)
(require 'server)

(let ((runtime-dir (getenv "XDG_RUNTIME_DIR"))
      (pid (number-to-string (emacs-pid))))
  (setq server-name (concat server-name "-" (number-to-string (emacs-pid))))
  (setenv "EMACS_SERVER" server-name)
  (when (and runtime-dir (string-equal (getenv "XDG_SESSION_TYPE") "wayland"))
    (add-hook 'delete-frame-functions
              (lambda (frame)
                (dolist (buffer (mapcar 'window-buffer (window-list frame 0)))
                  (when (and (buffer-live-p buffer)
                             (not (string-prefix-p "*" (buffer-name))))
                    (kill-buffer)))))
    (with-temp-buffer
      (insert pid)
      (write-file (concat runtime-dir "/emacs.pid")))))

(server-start)

;;; init.el ends here
