#!/usr/bin/python
import re
import subprocess
from inspect import getsourcefile
from os import environ
from os.path import abspath, dirname, join
from PyQt6.QtGui import QAction, QCursor, QIcon
from PyQt6.QtWidgets import QApplication, QMenu, QSystemTrayIcon

curr = dirname(abspath(getsourcefile(lambda:0)))
fixed = []
points = None

def item(label, task, icon = 'folder'):
  if task == True:
    action = QAction(QIcon.fromTheme(icon), u'挂载 ' + label[:10])
    action.triggered.connect(lambda *_: subprocess.call(['gio', 'mount', '-d', points[label]]))
  elif task == False:
    action = QAction(QIcon.fromTheme(icon), u'卸载 ' + label[:10])
    action.triggered.connect(lambda *_: subprocess.call([join(curr, "unmount.sh"), points[label]]))
  elif isinstance(task, str):
    action = QAction(QIcon.fromTheme(icon), label)
    action.triggered.connect(lambda *_: subprocess.call([join(curr, "emacsclient.sh"), "-n", task]))
  elif isinstance(task, list):
    action = QAction(QIcon.fromTheme(icon), label)
    action.triggered.connect(lambda *_: subprocess.call([join(curr, "emacsclient.sh"), "-n"] + task))
  else:
    action = QAction(QIcon.fromTheme(icon), label)
    action.triggered.connect(task)
  return action

class MyTray(QSystemTrayIcon):
  def __init__(self):
    super().__init__(QIcon.fromTheme('system-file-manager'))
    self.activated.connect(lambda reason: self.on_left_click(reason))

  def on_left_click(self, reason):
    if reason == QSystemTrayIcon.ActivationReason.Trigger:
      label = ''
      items = []
      opens = []
      global points
      points = dict()
      mounts = subprocess.run(['gio', 'mount', '-l', '-i'], capture_output = True)
      for line in mounts.stdout.decode('utf-8').splitlines():
        line = line.strip()
        m = re.search(r'^((Volume)|(Mount))[^:]+:(.+)$', line)
        if m != None:
          label = re.split('->', m.group(4))[0].strip()
        else:
          m = re.search(r'^((uuid)|(default_location))=(.+)$', line)
          if m != None:
            points[label] = m.group(4)
          elif line == 'can_mount=1':
            items.append(item(label, True, 'drive-harddisk'))
          elif line == 'can_unmount=1':
            items.append(item(label, False, 'media-eject'))
            point = points[label]
            if point.startswith('file://'):
              opens.append(item(u'打开 ' + label, point[7:]))

      menu = QMenu()
      for action in fixed + items + opens:
        action.setParent(menu)
        menu.addAction(action)
      self.setContextMenu(menu)
      pos = QApplication.screenAt(QCursor.pos()).geometry().topRight()
      self.contextMenu().popup(pos)

def main():
  global fixed
  app = QApplication([])
  app.setQuitOnLastWindowClosed(False)
  fixed.append(item(u'打开主目录', '~', 'user-home'))
  can = join(join(environ['XDG_DATA_HOME'], 'Trash'), 'files')
  fixed.append(item(u'打开回收站', ['-e', f'(find-file "{can}")'], 'user-trash-full'))
  fixed.append(item(u'清空回收站', lambda *_: subprocess.call(['gio', 'trash', '--empty']), 'user-trash'))
  tray = MyTray()
  tray.setVisible(True)
  app.exec()

if __name__ == '__main__':
  main()
