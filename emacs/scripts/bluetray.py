#!/usr/bin/python
import re
import subprocess
import sys
from PyQt6.QtGui import QAction, QCursor, QIcon
from PyQt6.QtWidgets import QApplication, QMenu, QSystemTrayIcon

points = None

def item(label, connected):
  if connected == True:
    action = QAction(QIcon.fromTheme('bluetooth-active'), label)
    action.triggered.connect(lambda *_: subprocess.call(['bluetoothctl', 'disconnect', points[label]]))
  else:
    action = QAction(QIcon.fromTheme('bluetooth-disabled'), label)
    action.triggered.connect(lambda *_: subprocess.call(['bluetoothctl', 'connect', points[label]]))
  return action

class MyTray(QSystemTrayIcon):
  def __init__(self):
    super().__init__(QIcon.fromTheme('bluetooth'))
    self.activated.connect(lambda reason: self.on_left_click(reason))

  def on_left_click(self, reason):
    if reason == QSystemTrayIcon.ActivationReason.Trigger:
      label = ''
      items = []
      global points
      points = dict()
      devices = subprocess.run(['bluetoothctl', 'devices'], capture_output = True)
      for line in devices.stdout.decode('utf-8').splitlines():
        line = line.strip()
        m = re.search(r'^Device (\S+) (.+)$', line)
        if m != None:
          label = m.group(2)
          point = m.group(1)
          points[label] = point
          info = subprocess.run(['bluetoothctl', 'info', point], capture_output = True)
          for l in info.stdout.decode('utf-8').splitlines():
            l = l.strip()
            n = re.search(r'^Connected: (.+)$', l)
            if n != None:
              items.append(item(label, n.group(1) == 'yes'))
              break

      menu = QMenu()
      for action in items:
        action.setParent(menu)
        menu.addAction(action)
      self.setContextMenu(menu)
      pos = QApplication.screenAt(QCursor.pos()).geometry().topRight()
      self.contextMenu().popup(pos)

def main():
  app = QApplication([])
  app.setQuitOnLastWindowClosed(False)
  tray = MyTray()
  tray.setVisible(True)
  app.exec()

if __name__ == '__main__':
  main()
