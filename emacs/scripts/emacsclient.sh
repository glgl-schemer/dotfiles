#!/bin/sh

if [ -n "$EMACS_SERVER" ]; then
    emacsclient -s "$EMACS_SERVER" "$@"
else
    emacsclient -s server-"$(cat "$XDG_RUNTIME_DIR"/emacs.pid)" -r "$@"
fi
