#!/bin/sh

swayidle \
    timeout 5 'swaymsg "output * power off"' \
    resume 'swaymsg "output * power on"' &

#shellcheck disable=SC2064
trap "kill $!" HUP INT TERM EXIT ABRT

swaylock "$@"
