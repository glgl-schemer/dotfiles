#!/bin/sh

content="$(cat "$@")"
[ -z "$content" ] && exit
pipe="/tmp/pipe-$$"
mkfifo "$pipe"
printf '%s' "$content" > "$pipe" &
emacsclient -s "$EMACS_SERVER" -que '
(make-process :name "eless" :noquery t
              :connection-type '\''pipe :command '\''("cat" "'"$pipe"'")
              :buffer (generate-new-buffer (concat "*eless: " default-directory "*"))
              :sentinel
              (lambda (proc _event)
                   (when (eq '\''exit (process-status proc))
                       (delete-file "'"$pipe"'")
                       (with-current-buffer (process-buffer proc)
                         (if (not (string-empty-p "'"$MAN_PN"'"))
                             (progn (goto-char (point-min))
                                    (if (re-search-forward "^[.'\'']" 1000 t)
                                        (woman-decode-buffer)
                                      (woman-man-buffer)
                                      (ansi-color-apply-on-region
                                       (point-min) (point-max))))
                           (ansi-color-apply-on-region (point-min) (point-max)))
                         (read-only-mode)
                         (goto-char (point-min))
                         (rename-buffer (concat "eless: " default-directory) t)
                         (switch-to-buffer (current-buffer))))))
'
