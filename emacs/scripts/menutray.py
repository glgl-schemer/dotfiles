#!/usr/bin/python
import os
import re
import shlex
import subprocess
from PyQt6.QtGui import QAction, QCursor, QIcon
from PyQt6.QtWidgets import QApplication, QMenu, QSystemTrayIcon
from xdg.DesktopEntry import DesktopEntry as xdgentry
from xdg.BaseDirectory import xdg_data_dirs

items = dict()
icons = {
  'Accessories': 'Accessories',
  'Audio': 'Multimedia',
  'AudioVideo': 'Multimedia',
  'Development': 'Development',
  'Engineering': 'Engineering',
  'Games': 'Games',
  'Graphics': 'Graphics',
  'Internet': 'Internet',
  'Multimedia': 'Multimedia',
  'Network': 'Internet',
  'Office': 'Office',
  'Player': 'Multimedia',
  'Science': 'Science',
  'System': 'System',
  'TextEditor': 'Development',
  'TV': 'Multimedia',
  'Utility': 'Utilities',
  'Video': 'Multimedia',
}

class MyTray(QSystemTrayIcon):
  def __init__(self):
    super().__init__(QIcon.fromTheme('application-menu'))
    self.activated.connect(lambda reason: self.on_left_click(reason))

  def on_left_click(self, reason):
    if reason == QSystemTrayIcon.ActivationReason.Trigger:
      pos = QApplication.screenAt(QCursor.pos()).geometry().topRight()
      self.contextMenu().popup(pos)

def item(label, icon, task = True):
  if task == True:
    wenv = os.environ.copy()
    wenv['QT_QPA_PLATFORM'] = 'wayland;xcb'
    task = lambda *_: subprocess.Popen(shlex.split(items[label]), env = wenv, start_new_session = True)
  action = QAction(QIcon.fromTheme(icon), label)
  if task: action.triggered.connect(task)
  return action

def main():
  app = QApplication([])
  app.setQuitOnLastWindowClosed(False)

  submenus = dict()
  for dir in reversed(xdg_data_dirs):
    if os.path.exists(os.path.join(dir, 'applications')):
      for file in os.listdir(os.path.join(dir, 'applications')):
        filename = os.fsdecode(file)
        if filename.endswith('.desktop'):
          entry = xdgentry(filename = os.path.join(dir, 'applications', filename))
          if entry.getName() not in items:
            label = None
            for category in entry.getCategories():
              if label := icons.get(category): break
            label = label if label else 'Other'
            submenu = submenus.setdefault(label, [])
            icon = entry.getIcon() if entry.getIcon() else 'unknown'
            submenu.append(item(entry.getName(), icon))
          items[entry.getName()] = re.sub(r'%[a-zA-Z]', '', entry.getExec()).replace('\\\\', '\\').replace('\\$', '$')

  menu = QMenu()
  for label, submenu in sorted(submenus.items()):
    sortedmenu = QMenu(menu)
    for child in sorted(submenu, key = lambda child: child.text()):
      child.setParent(sortedmenu)
      sortedmenu.addAction(child)
    action = item(label, 'applications-' + label.lower(), False)
    action.setMenu(sortedmenu)
    action.setParent(menu)
    menu.addAction(action)

  logout = lambda *_: subprocess.call(['swaymsg', 'exit'])
  logout_action = item('Logout', 'system-log-out', logout)
  logout_action.setParent(menu)
  menu.addAction(logout_action)

  tray = MyTray()
  tray.setVisible(True)
  tray.setContextMenu(menu)
  app.exec()

if __name__ == '__main__':
  main()
