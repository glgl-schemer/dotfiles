#!/bin/sh

content="$(cat)"
[ -z "$content" ] && exit
pipe="/tmp/pipe-$$"
mkfifo "$pipe"
printf '%s' "$content" > "$pipe" &
emacsclient -s "$EMACS_SERVER" -que '
(make-process :name "esel" :noquery t
              :connection-type '\''pipe :command '\''("cat" "'"$pipe"'")
              :buffer (generate-new-buffer "*sel*")
              :sentinel
              (lambda (proc _event)
                   (when (eq '\''exit (process-status proc))
                       (delete-file "'"$pipe"'")
                       (with-current-buffer (process-buffer proc)
                         (kill-new (buffer-string))
                         (kill-buffer)))))
'
