#!/bin/sh

curr="$(dirname "$0")"

gio mount -u "$1"

dir="${1#file://}"

[ "$dir" = "$1" ] || [ -r "$1" ] || {
    "$curr"/emacsclient.sh -n -e "
    (dolist (buffer (buffer-list))
      (with-current-buffer buffer
        (when (and (derived-mode-p 'dired-mode)
                   (string-prefix-p \"${dir}\" default-directory))
          (kill-buffer))))
"
}
