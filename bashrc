# -*- mode: sh -*-
case $- in *i*) ;; *) return ;; esac

vterm_printf() {
    if [ -n "$TMUX" ] && { [ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ]; }; then
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

vterm_prompt_end() {
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
}

prompt_print() {
    printf "\033]0;%s\007" "$(whoami)@$(hostname):$([ "$HOME" = "$PWD" ] && printf \~ && exit; basename "$PWD")"
}

stty -ixon

if [ "$INSIDE_EMACS" = "vterm" ]; then
    alias clear='vterm_printf "51;Evterm-clear-scrollback"; tput clear'
    alias clear-screen=clear
    if [ -n "$PROMPT_COMMAND" ]; then
        export PROMPT_COMMAND="${PROMPT_COMMAND}; prompt_print"
    else
        export PROMPT_COMMAND='prompt_print'
    fi
    PS1="$PS1"'\[$(vterm_prompt_end)\]'
fi

if [ -n "$INSIDE_EMACS" ]; then
    export EDITOR="emacsclient -s $EMACS_SERVER"
    export PAGER="$HOME/.config/emacs/scripts/eless.sh"
    alias less='$HOME/.config/emacs/scripts/eless.sh'
    alias paru='paru --fm $HOME/.config/emacs/scripts/emacsclient.sh'
fi

if [ "$TERM" = "dumb" ]; then
    PS1="> "
fi
